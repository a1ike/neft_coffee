"use strict";

$(document).ready(function () {
  $(window).on('load', function () {
    var progress = $('#bar1');
    progress.animate({
      width: '0%'
    }, 200, function () {
      setTimeout(function () {
        $('.bg_loader, .progress').fadeOut(800);
      }, 200);
    });
  });
  $(window).scroll(function () {
    if ($(window).scrollTop() >= 1) {
      $('.top_sidebar').addClass('fixed');
    } else {
      $('.top_sidebar').removeClass('fixed');
    }
  }); //slider autoplay 

  setInterval(function () {
    if (!$('.slick-slider:hover').length > 0) {
      $('.slick-next').trigger('click');
    }
  }, 4000); //slider ---

  function changeCurrent(slider) {
    var sliders = slider.find('.slick-track').children();
    var circle = $('#circlebg');
    var slideCurent = $('.slide_curent');
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;

    for (var i = 0; i < sliders.length; i++) {
      if (sliders.eq(i).hasClass('slick-current')) {
        slideCurent.html(i + 1);
        var offset = 938 + sectorsNumber * (i + 1);
        circle.css('stroke-dashoffset', offset);
      }
    }
  }

  $('.front_slider').on('init', function (event, slick) {
    var counterAll = $('.slide_all');
    var slider = $('.front_slider .slick-track');
    var sliderLength = slider.children().length;
    counterAll.html(sliderLength); //circle

    var sliders = $('.front_slider .slick-track').children();
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;
    var circle = $('#circlebg');
    var offset = 938 + sectorsNumber;
    circle.css('stroke-dashoffset', offset);
  });
  $('.front_slider').click(function () {
    changeCurrent($(this));
  });
  $('.front_slider').on('swipe', function (event, slick, currentSlide, nextSlide) {
    changeCurrent($(this));
  });
  $('.front_slider').on('init', function (ev, el) {
    $('video').each(function () {
      this.play();
    });
  });
  $('.front_slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    fade: true,
    cssEase: 'ease-in-out',
    speed: 500
  });
  $('.places_slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    autoplay: false,
    fade: true,
    cssEase: 'ease-in-out',
    speed: 500
  }); //---
  //toggler

  $('.menu').click(function () {
    $(this).toggleClass('menu_active');
    $('#toggle').toggleClass('on');
    $('.toggle_mnu_wrapp').toggleClass('active');
    $('body').toggleClass('overflow');
    $('html').toggleClass('overflow');
  });
  $('.toggle_mnu_wrapp').click(function (e) {
    console.log('asd', $(e.target));

    if ($(e.target).hasClass('toggle_mnu_wrapp')) {
      $('#toggle').removeClass('on');
      $('.toggle_mnu_wrapp').removeClass('active');
      $('body').removeClass('overflow');
      $('.menu').removeClass('menu_active');
      $('html').removeClass('overflow');
    }
  }); // $(window).on('scroll', function() {
  // 	$('#toggle').removeClass("on");
  // 	$(".toggle_mnu_wrapp").removeClass("active");
  // 	$(".menu").removeClass("menu_active");
  // });
  //---
  //main_form_pop_up

  $('.live_order_main, .consalting, .live_order, .barista_botton').click(function (e) {
    e.preventDefault();
    $('.main_form_pop_up_bg').fadeIn();
    $('body').addClass('overflow');
    $('html').addClass('overflow');
  });
  $('.main_form_pop_up_close ').click(function () {
    $('.main_form_pop_up_bg').fadeOut();
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
  }); //E-mail Ajax Send

  $('input[name$=\'admin_email\']').val('mail@neftcofee.ru');
  $('.m_form').submit(function () {
    //Change
    var th = $(this);
    $.ajax({
      type: 'POST',
      url: '/mail.php',
      //Change
      data: th.serialize()
    }).done(function () {
      $('.m_form').find('button').html('Thank You');
      setTimeout(function () {
        // Done Functions
        $('.m_form').find('button').html('ÐžÑÑ‚Ð°Ð²Ð¸Ñ‚ÑŒ Ð·Ð°ÑÐ²ÐºÑƒ');
        $('.main_form_pop_up_bg').fadeOut();
        $('body').removeClass('overflow');
        $('html').removeClass('overflow');
        th.trigger('reset');
      }, 1000);
    });
    return false;
  });
  $('#franch_6_form').submit(function () {
    //Change
    var th = $(this);
    $.ajax({
      type: 'POST',
      url: '/mail.php',
      //Change
      data: th.serialize()
    }).done(function () {
      $('#franch_6_form').find('button').html('Thank You');
      setTimeout(function () {
        // Done Functions
        $('#franch_6_form').find('button').html('ÐžÑÑ‚Ð°Ð²Ð¸Ñ‚ÑŒ Ð·Ð°ÑÐ²ÐºÑƒ');
        $('.main_form_pop_up_bg').fadeOut();
        $('body').removeClass('overflow');
        $('html').removeClass('overflow');
        th.trigger('reset');
      }, 1000);
    });
    return false;
  });
  $('.barista_popup_form_val').submit(function () {
    //Change
    var th = $(this);
    $.ajax({
      type: 'POST',
      url: '/mail.php',
      //Change
      data: th.serialize()
    }).done(function () {
      $('.barista_popup_form_val').find('button').html('Thank You');
      setTimeout(function () {
        // Done Functions
        $('.barista_popup_form_val').find('button').html('ÐžÑÑ‚Ð°Ð²Ð¸Ñ‚ÑŒ Ð·Ð°ÑÐ²ÐºÑƒ');
        $('.main_form_pop_up_bg').fadeOut();
        $('body').removeClass('overflow');
        $('html').removeClass('overflow');
        th.trigger('reset');
      }, 1000);
    });
    return false;
  }); //E-mail Ajax Send

  $('#contact_main_form').submit(function () {
    //Change
    var th = $(this);
    $.ajax({
      type: 'POST',
      url: '/mail.php',
      //Change
      data: th.serialize()
    }).done(function () {
      $('#contact_main_form').find('button').html('Thank You');
      setTimeout(function () {
        // Done Functions
        $('#contact_main_form').find('button').html('ÐžÑ‚Ð¿Ñ€Ð°Ð²Ð¸Ñ‚ÑŒ');
        th.trigger('reset');
      }, 1000);
    });
    return false;
  });
});
$(document).ready(function () {
  //slider ---
  function changeCurrent(slider) {
    var sliders = slider.find('.slick-track').children();
    var circle = $('#circlebg');
    var slideCurent = $('.slide_curent');
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;

    for (var i = 0; i < sliders.length; i++) {
      if (sliders.eq(i).hasClass('slick-current')) {
        slideCurent.html(i + 1);
        var offset = 938 + sectorsNumber * (i + 1);
        circle.css('stroke-dashoffset', offset);
      }
    }
  } //franch page


  $('.abou_history_slider').on('init', function (event, slick) {
    var counterAll = $('.abou_history_right .slide_all');
    var slider = $('.abou_history_slider .slick-track');
    var sliderLength = slider.children().length;
    counterAll.html(sliderLength); //circle

    var sliders = $('.abou_history_slider .slick-track').children();
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;
    var circle = $('.abou_history_right #circlebg');
    var offset = 938 + sectorsNumber;
    circle.css('stroke-dashoffset', offset);
  });
  $('.abou_history_slider').click(function () {
    changeCurrent($(this));
  });
  $('.abou_history_slider').on('swipe', function (event, slick, currentSlide, nextSlide) {
    changeCurrent($(this));
  });
  $('.abou_history_slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    fade: true,
    cssEase: 'ease-in-out',
    speed: 500
  }); //franch page

  $('.n-teacher__cards').on('init', function (event, slick) {
    var counterAll = $('.abou_history_right .slide_all');
    var slider = $('.n-teacher__cards .slick-track');
    var sliderLength = slider.children().length;
    counterAll.html(sliderLength); //circle

    var sliders = $('.n-teacher__cards .slick-track').children();
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;
    var circle = $('.abou_history_right #circlebg');
    var offset = 938 + sectorsNumber;
    circle.css('stroke-dashoffset', offset);
  });
  $('.n-teacher__cards').click(function () {
    changeCurrent($(this));
  });
  $('.n-teacher__cards').on('swipe', function (event, slick, currentSlide, nextSlide) {
    changeCurrent($(this));
  });
  $('.n-teacher__cards').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    fade: true,
    cssEase: 'ease-in-out',
    speed: 500
  }); //fancy

  $('.fancy').fancybox({
    youtube: {
      controls: 1,
      showinfo: 0
    }
  });
});
$(document).ready(function () {
  //slider ---
  function changeCurrent(slider) {
    var sliders = slider.find('.slick-track').children();
    var circle = $('#circlebg');
    var slideCurent = $('.slide_curent');
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;

    for (var i = 0; i < sliders.length; i++) {
      if (sliders.eq(i).hasClass('slick-current')) {
        slideCurent.html(i + 1);
        var offset = 938 + sectorsNumber * (i + 1);
        circle.css('stroke-dashoffset', offset);
      }
    }
  } //franch page


  $('.franch_3_slider').on('init', function (event, slick) {
    var counterAll = $('.franch_3 .slide_all');
    var slider = $('.franch_3_slider .slick-track');
    var sliderLength = slider.children().length;
    counterAll.html(sliderLength); //circle

    var sliders = $('.franch_3_slider .slick-track').children();
    var dashOffset = 62;
    var sectorsNumber = dashOffset / sliders.length;
    var circle = $('.franch_3 #circlebg');
    var offset = 938 + sectorsNumber;
    circle.css('stroke-dashoffset', offset);
  });
  $('.franch_3_slider').click(function () {
    changeCurrent($(this));
  });
  $('.franch_3_slider').on('swipe', function (event, slick, currentSlide, nextSlide) {
    changeCurrent($(this));
  });
  $('.franch_3_slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    infinite: true,
    fade: true,
    speed: 500,
    asNavFor: '.franch_3_content_slider'
  });
  $('.franch_3_content_slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    infinite: true,
    fade: true,
    speed: 500,
    asNavFor: '.franch_3_slider'
  }); //tabs

  $('#franch_1').click(function () {
    $('.franch_nav a').removeClass('active');
    $('.franch_nav li').removeClass('liactive');
    $(this).addClass('active');
    $(this).parent().addClass('liactive');
    $('.franch').hide();
    $('.franch_1').show();
    $('#franch_1_mob').parent().addClass('liactive');
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
  });
  $('#franch_2, #franch_1_more').click(function () {
    $('.franch').hide();
    $('.franch_2').show();
    $('.franch_nav a').removeClass('active');
    $('.franch_nav li').removeClass('liactive');
    $(this).addClass('active'); // $(this).parent().addClass('liactive');

    $('#franch_2_mob').parent().addClass('liactive');
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
  });
  $('#franch_3').on('click', function () {
    $('.franch').hide();
    $('.franch_nav a').removeClass('active');
    $('.franch_nav li').removeClass('liactive');
    $(this).addClass('active');
    $('#franch_3_mob').parent().addClass('liactive');
    $('.franch_3').show();
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
    $('.franch_3_slider').slick('reinit');
    $('.franch_3_slider').slick('slickNext');
  });
  $('#franch_4').click(function () {
    $('.franch_nav a').removeClass('active');
    $('.franch_nav li').removeClass('liactive');
    $(this).addClass('active');
    $('#franch_4_mob').parent().addClass('liactive');
    $('.franch').hide();
    $('.franch_4').show();
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
  });
  $('#franch_5').click(function () {
    $('.franch_nav a').removeClass('active');
    $('.franch_nav li').removeClass('liactive');
    $(this).addClass('active');
    $('#franch_5_mob').parent().addClass('liactive');
    $('.franch').hide();
    $('.franch_5').show();
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
  });
  $('#franch_6').click(function () {
    $('.franch_nav a').removeClass('active');
    $('.franch_nav li').removeClass('liactive');
    $(this).addClass('active');
    $('#franch_6_mob').parent().addClass('liactive');
    $('.franch').hide();
    $('.franch_6').show();
    $('body').removeClass('overflow');
    $('html').removeClass('overflow');
  }); //---

  $('.franch_nav_mobile a').click(function (e) {
    $('.franch_nav').slideToggle();
    $('.franch_nav').addClass('franch_nav_open');
    $('.franch_nav_main a').addClass('check_franch');
    $('.franch_nav_main span').addClass('check_franch');
    $(this).addClass('active');
    $('body').addClass('overflow');
    $('html').addClass('overflow');
  });
  $('body').click(function (e) {
    if ($(e.target).hasClass('check_franch')) {
      $('.franch_nav_open').slideUp();
    }
  }); //franch-toggler

  $('.franch_5_right .show_580_flex').click(function () {
    console.log('ASDASD');
    $(this).toggleClass('franch_5_togler_active');
    $(this).next().slideToggle();
  });
  $('.franch_5_togler_first').click(function () {
    if (!$(this).hasClass('show_580_flex')) {
      if (!$(this).hasClass('franch_5_togler_active')) {
        $('.franch_5_more').slideUp();
        $('.franch_5_togler').removeClass('franch_5_togler_active');
        $(this).addClass('franch_5_togler_active');
        var toggleMore = $(this).parent().parent().find('.franch_5_more_1');
        toggleMore.slideDown();
      } else {
        $('.franch_5_more').slideUp();
        $('.franch_5_togler').removeClass('franch_5_togler_active');
      }
    } // if ($('.franch_5_togler').hasClass("franch_5_togler_active")) {
    // 	$('.franch_5_more_1').slideUp();
    // 	console.log('asdas')
    // }	

  });
  $('.franch_5_togler_second').click(function () {
    if (!$(this).hasClass('show_580_flex')) {
      if (!$(this).hasClass('franch_5_togler_active')) {
        $('.franch_5_more').slideUp();
        $('.franch_5_togler').removeClass('franch_5_togler_active');
        $(this).addClass('franch_5_togler_active');
        var toggleMore = $(this).parent().parent().find('.franch_5_more_2');
        toggleMore.slideDown();
      } else {
        $('.franch_5_more').slideUp();
        $('.franch_5_togler').removeClass('franch_5_togler_active');
      }
    }
  }); // mobile 

  if ($('body').width() <= 580) {
    $('.franch_3_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      infinite: true,
      fade: true,
      cssEase: 'ease-in-out',
      speed: 500,
      asNavFor: '.franch_3_content_slider'
    });
    $('.franch_3_content_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      infinite: true,
      fade: true,
      cssEase: 'ease-in-out',
      speed: 500,
      asNavFor: '.franch_3_slider'
    });
  }
});
$(window).scroll(function () {
  if ($(window).scrollTop() >= 1) {
    $('.top_sidebar').addClass('fixed');
  } else {
    $('.top_sidebar').removeClass('fixed');
  }
});
$(document).ready(function () {
  /* $('.phone').inputmask('+7(999)999-99-99'); */
  $('.n-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.n-nav').toggleClass('n-nav_active');
  });
  $(document).click(function (event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if ($(event.target).is('.n-header__toggle') || $(event.target).is('.n-header__toggle img')) {
      console.log('');
    } else if ($(event.target).is('.n-nav')) {
      console.log('');
    } else {
      $('body').find('.n-nav').removeClass('n-nav_active');
    }
  });
  /* $(document).click(function(e) {
    if ($('.n-nav').hasClass('n-nav_active')) {
      $('.n-nav').removeClass('n-nav_active');
    }
  }); */

  $('.n-features-seo-collapse__header').on('click', function (e) {
    e.preventDefault();
    $('.n-features-seo-collapse__header_active').removeClass('n-features-seo-collapse__header_active').next().slideToggle('fast');
    $(this).toggleClass('n-features-seo-collapse__header_active');
    $(this).next().slideToggle('fast');
  });
  $('.n-filter__title').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('n-filter__title_active');
    $(this).next().slideToggle('fast');
  });
  $('.n-basket-card__counter-minus').on('click', function (e) {
    e.preventDefault();
    var itemPrice = +$(this).parent().parent().parent().find('.n-basket-card__price span').html();
    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this).next().val(+currentVal - 1);
      $(this).parent().parent().parent().find('.n-basket-card__summ span').html(itemPrice * (+currentVal - 1));
    }
  });
  $('.n-basket-card__counter-plus').on('click', function (e) {
    e.preventDefault();
    var itemPrice = +$(this).parent().parent().parent().find('.n-basket-card__price span').html();
    var currentVal = $(this).prev().val();
    $(this).prev().val(+currentVal + 1);
    $(this).parent().parent().parent().find('.n-basket-card__summ span').html(itemPrice * (+currentVal + 1));
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.n-modal').slideToggle('fast');
  });
  $('.n-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.n-modal').slideToggle('fast');
  });
  $('.open-info').on('click', function (e) {
    e.preventDefault();
    $(this).parents('.n-school-levels__card').find('.n-school__info').toggle();
    $(this).closest('.n-school-levels__card-content').toggle();
  });
  $('.open-form').on('click', function (e) {
    e.preventDefault();
    $(this).parents('.n-school-levels__card').find('.n-school__form').toggle();
    $(this).closest('.n-school-levels__card-content').toggle();
  });
  $('.n-school__close').on('click', function (e) {
    e.preventDefault();
    $(this).parent().toggle();
    $(this).parent().parent().find('.n-school-levels__card-content').toggle();
  });
  new Swiper('.n-goods-big', {
    pagination: {
      el: '.n-goods-big .swiper-pagination',
      type: 'fraction'
    },
    navigation: {
      nextEl: '.n-goods-big .swiper-button-next',
      prevEl: '.n-goods-big .swiper-button-prev'
    }
  });
  var mySwiper = new Swiper('.n-teacher__cards', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    pagination: {
      el: '.n-teacher__cards .swiper-pagination',
      type: 'fraction'
    },
    navigation: {
      nextEl: '.n-teacher__cards .swiper-button-next',
      prevEl: '.n-teacher__cards .swiper-button-prev'
      /* ,
          on: {
            init: function() {
              var dashOffset = 62;
              var sectorsNumber = dashOffset / +$('.swiper-pagination-total').html();
              var circle = $('#circlebg');
              var offset = 938 + sectorsNumber;
              circle.css('stroke-dashoffset', offset);
            },
          }, */

    }
  });
  /* mySwiper.on('slideChange', function() {
    var circle = $('#circlebg');
    var dashOffset = 62;
    var sectorsNumber = dashOffset / (+$('.swiper-pagination-total').html());
      for (var i = 1; i < +$('.swiper-pagination-total').html(); i++) {
      var offset = 938 + sectorsNumber * (+$('.swiper-pagination-total').html() + 1);
      circle.css('stroke-dashoffset', offset);
    }
  }); */

  jQuery(function () {
    jQuery('#bgndVideo').YTPlayer({
      useOnMobile: false,
      host: 'https://www.youtube.com'
    });
    jQuery('#bgndVideoTwo').YTPlayer({
      useOnMobile: false,
      host: 'https://www.youtube.com'
    });
    jQuery('#bgndVideoThree').YTPlayer({
      useOnMobile: false,
      host: 'https://www.youtube.com'
    });
  });
  $('.phone').inputmask('+7(999)-999-99-99');
  $('input[name="basket_region"]').on('input, change', function (e) {
    if ($(this).val() === 'Казань') {
      $('.n-basket-form-get-kazan').hide();
    } else if ($(this).val() === 'Россия') {
      $('.n-basket-form-get-kazan').show();
    }
  });
  $('input[name="basket_delivery"]').on('input, change', function (e) {
    if ($(this).val() === 'Постамат') {
      $('.n-basket-form-get-post').toggle();
    } else if ($(this).val() === 'Служба') {
      $('.n-basket-form-get-post').toggle();
    }
  });
  $('input[name="basket_type"]').on('input, change', function (e) {
    if ($(this).val() === 'Доставка') {
      $('.n-basket-form-get-take').hide();
      $('.n-basket-form-get-order').show();
    } else if ($(this).val() === 'Забрать') {
      $('.n-basket-form-get-order').hide();
      $('.n-basket-form-get-take').show();
    }
  });
});
new WOW().init();
//# sourceMappingURL=main.js.map
